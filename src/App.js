import React, { Component } from 'react';
import './App.css';
import Chat from "./container/chat/Chat";

class App extends Component {
  render() {
    return (
      <Chat />
    );
  }
}

export default App;
