import React, {Component} from 'react';
import PostForm from "../../components/PostForm/PostForm";
import PostList from "../../components/PostList/PostList";
import './Chat.css';


class Chat extends Component {
    state = {
        messageList: [],
        author: '',
        message: ''
    };

    componentDidMount() {
        this.getMessages();
    }

    getMessages = () => {
        const MESSAGES_URL = 'http://146.185.154.90:8000/messages';
        fetch(MESSAGES_URL).then(response => response.json()).then(response => {
            console.log(response)
            let lastIndex = response[response.length - 1];
            this.setState({messageList: response})

            setInterval(() => {
                fetch(MESSAGES_URL + '?datetime=' + lastIndex.datetime).then(response => response.json()).then(response => {
                    console.log(response)
                    if (response.length !== 0) {
                        lastIndex = response[response.length - 1];
                        const updatedMessages = [...this.state.messageList]
                        updatedMessages.push(lastIndex)
                        this.setState({messageList: updatedMessages})
                        console.log(lastIndex)
                    }


                })
            }, 2000)
        })
    }

    enterMessages = () => {
        let form = new URLSearchParams();
        form.append('message', this.state.message);
        form.append('author', this.state.author);

        fetch("http://146.185.154.90:8000/messages", {
            method: "POST",
            body: form
        });
    };

    changeAuthor = (event) => {
        this.setState({author: event.target.value})
    };

    changeMessage = (event) => {
        this.setState({message: event.target.value})
    };


    render() {
        return (
            <div className="Chat">
                <PostForm
                    changeAuthor={this.changeAuthor}
                    changeMessage={this.changeMessage}
                    clicked={this.enterMessages}
                />
                <PostList messages={this.state.messageList}/>
            </div>
        )
    }
}

export default Chat;