import React, {Component} from 'react';
import './PostList.css';
import Moment from 'react-moment';
import 'moment-timezone';


const PostList = props => {
    return (
        <div className="PostList">
            {props.messages.map(message => (
                <div key={message._id}>
                    <span > {message.author}</span>
                    <span > {message.message}</span>
                    <span > {message.datetime}</span>
                </div>
            )).reverse()}
        </div>
    )
};

export default PostList;


