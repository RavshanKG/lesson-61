import React from 'react';
import './PostForm.css';


const PostForm = (props) => {
        return (
            <div>
                <input className="Input" placeholder="Name" type="text" value={props.author} onChange={props.changeAuthor}/>
                <button className="Button" onClick={props.clicked}>Enter</button>
                <textarea className="Textarea" id="textarea" placeholder="Enter message" value={props.message} onChange={props.changeMessage}> </textarea>

            </div>
        )
    }

export default PostForm;